import { Router } from 'express';
import * as personaController from '../controllers/personas.controller';
import { body } from 'express-validator';
import { validPersonaEmail } from '../helper/db-validator';
import { validateFields } from '../middlewares/validate-fields';

const router = Router();


router.post('/login', personaController.login);
router.post('/', [
    body("email","Debe ser un Email valido").isString().isEmail(),
    body("contraseña","La contraseña es obligatoria").isString(),
    body("telefono","El telefono es obligatorio").isNumeric(),
    body("rol","El rol debe ser admin/cliente").isString().isIn(['admin', 'cliente']),
    body('email').custom(validPersonaEmail),
    validateFields
    ],personaController.store);

router.get('/', personaController.index);
router.get('/:id', personaController.show);
router.put('/:id',[
    body("nombreCompleto","El nombre es obligatorio").isString(),
    body("email","Debe ser un Email valido").isString().isEmail(),
    body("contraseña","La contraseña es obligatoria").isString(),
    body("telefono","El telefono es obligatorio").isNumeric(),
    body("rol","El rol debe ser admin/cliente").isString().isIn(['admin', 'cliente']),
    validateFields],
    personaController.update);
router.delete('/:id', personaController.destroy);


export default router;